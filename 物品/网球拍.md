# 网球球拍（含 3 个网球）

## 实物展示

![网球球拍](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_135918.webp)

<center>（图 1：球拍与球）</center>

![网球球拍](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_135948.webp)

<center>（图 2：拍柄，新缠的带子）</center>

![网球球拍](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_164320.webp)

<center>（图 3：球拍细节）</center>

## 说明

- **产品型号**：碳素网球拍
- **成色**：九成新，无磕碰
- **使用时长**：几次（ennn…
- **原价**：一百多块
- **赠品**：袋子，三个网球

## 报价

### 淘宝报价

- **淘宝相似产品**：[天龙碳素网球拍 单人初学者网球训练器套装男女大学生双人全专业](https://detail.tmall.com/item.htm?id=529340494776&spm=a1z09.2.0.0.4bd02e8dZ8n6yV&_u=o2co36g9e67f)
- **报价**：130-170 元

### 本商品报价

- **报价**：50 元
- **报价说明**：就玩过几次就又半价了，唉……有兴趣选网球课的小伙伴，以及有兴趣尝试一下的切莫错过！！
