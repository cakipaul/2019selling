# 镂花垃圾桶

## 实物展示

![镂花垃圾桶](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_163520.webp)

<center>（图 1：正视图）</center>

![镂花垃圾桶](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_163503.webp)

<center>（图 2：俯视图）</center>

![镂花垃圾桶](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_163536.webp)

<center>（图 3：底部商标）</center>

## 说明

- **成色**：基本全新，商标尚在，无磕碰
- **原价**：20 元左右
- **赠品**：无（垃圾袋正好用完了，就没得送了）
- **说明**：从图 2 可以看到，上方的环可以压住垃圾袋，压好后非常整齐（阿姨压一压~

## 报价

### 淘宝报价

- **淘宝相似产品**：[垃圾桶家用大号压圈](https://item.taobao.com/item.htm?spm=a230r.1.999.198.3658523chMxmNV&id=593453922177&ns=1#detail)
- **报价**：8 元（邮费 50 元？？）

还有一些包邮的，一般多个起售，单价 15 元左右。

### 本商品报价

- **报价**：8 元
- **报价说明**：可能有同学担心它的卫生状态，俺保障已经洗的干净的可以泡面了！（不过镂空的也没法泡hh ~ 配图上可以看到一些水渍还没干，而且现在它没放在地上，而是放在纸板上！！
