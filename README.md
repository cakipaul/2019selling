# 请先看公告

>**建议：使用电脑进行浏览**

## 报价、出售情况

### 生活用品

物品名|预览图|报价（元）|淘宝参考|出售情况
--|--|--|--|--
[床边收纳篮](https://cakipaul.gitlab.io/2019selling/物品/床边收纳篮.html)|![床边收纳篮](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_163731.webp)|8|17.4|
[品质泡面锅（已消毒）](https://cakipaul.gitlab.io/2019selling/物品/品质泡面锅.html)|![品质泡面锅](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_142334.webp)|10|20|
[地毯（已消毒）](https://cakipaul.gitlab.io/2019selling/物品/地毯.html)|![毯子](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_163648.webp)|10|30|
[夏季清凉弹性坐垫（已消毒）](https://cakipaul.gitlab.io/2019selling/物品/弹性坐垫.html)|![弹性坐垫](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_163630.webp)|39|90|
[极简垃圾桶（已消毒）](https://cakipaul.gitlab.io/2019selling/物品/垃圾桶2.html)|![极简垃圾桶](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_162905.webp)|8|22|
[镂花垃圾桶（已消毒）](https://cakipaul.gitlab.io/2019selling/物品/垃圾桶1.html)|![镂花垃圾桶](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_163520.webp)|5|15|已单卖
[收纳盒（少一个扣子）](https://cakipaul.gitlab.io/2019selling/物品/收纳盒.html)|![收纳盒](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_140546.webp)|8|25-40|已单卖
[吉他](https://cakipaul.gitlab.io/2019selling/物品/吉他.html)|![吉他](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/guitar.webp)|600|1500-1700|已售出

>注：点击物品名即可快速跳转到详情页面

### 运动用品

物品名|预览图|报价（元）|淘宝参考|出售情况
--|--|--|--|--
[握力器（一只）](https://cakipaul.gitlab.io/2019selling/物品/握力器.html)|![握力器](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_163418.webp)|10（一个）|30|
[跳绳](https://cakipaul.gitlab.io/2019selling/物品/跳绳.html)|![跳绳](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_163403.webp)|8|15-24|
[小哑铃（2.74kg）](https://cakipaul.gitlab.io/2019selling/物品/小哑铃.html)|![小哑铃](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_141149.webp)|20（一个）|40|
[大哑铃（22kg）](https://cakipaul.gitlab.io/2019selling/物品/大哑铃.html)|![大哑铃](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_141612.webp)|120|200|
[臂力棒（40kg）](https://cakipaul.gitlab.io/2019selling/物品/臂力棒.html)|![臂力棒](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_140115.webp)|10|30|
[头带](https://cakipaul.gitlab.io/2019selling/物品/头带.html)|![头带](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_163203.webp)|2|5|已单卖
[羽毛球拍](https://cakipaul.gitlab.io/2019selling/物品/羽毛球拍.html)|![羽毛球拍](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_140402.webp)|60（一对）|120|已单卖
[网球拍](https://cakipaul.gitlab.io/2019selling/物品/网球拍.html)|![网球球拍](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/IMG_20190624_135918.webp)|50|130-170|已单卖
[滑板](https://cakipaul.gitlab.io/2019selling/物品/滑板.html)|![滑板](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/skateboard.webp)|90|两百左右|已售出
[篮球与排球](https://cakipaul.gitlab.io/2019selling/物品/球.html)|![篮球与排球](https://dev.tencent.com/u/paulsun/p/2019selling/git/raw/master/pics/balls.webp)|180|五六百|已售出

>注：点击物品名即可快速跳转到详情页面

## 优惠套餐

套餐名|内含|报价（元）|淘宝参考|出售情况
--|--|--|--|--
舒服宿舍|[品质泡面锅](https://cakipaul.gitlab.io/2019selling/物品/品质泡面锅.html)+[地毯](https://cakipaul.gitlab.io/2019selling/物品/地毯.html)+[坐垫](https://cakipaul.gitlab.io/2019selling/物品/弹性坐垫.html)+[极简垃圾桶](https://cakipaul.gitlab.io/2019selling/物品/垃圾桶2.html)+[握力器](https://cakipaul.gitlab.io/2019selling/物品/握力器.html)|**55**（~~79~~）|192|
健身宿舍|[小哑铃](https://cakipaul.gitlab.io/2019selling/物品/小哑铃.html)+[大哑铃](https://cakipaul.gitlab.io/2019selling/物品/大哑铃.html)+[臂力棒](https://cakipaul.gitlab.io/2019selling/物品/臂力棒.html)+[握力器](https://cakipaul.gitlab.io/2019selling/物品/握力器.html)|**138**（~~160~~）|300|
收纳之星|[收纳盒](https://cakipaul.gitlab.io/2019selling/物品/收纳盒.html)+[床边收纳篮](https://cakipaul.gitlab.io/2019selling/物品/床边收纳篮.html)+[镂花垃圾桶](https://cakipaul.gitlab.io/2019selling/物品/垃圾桶1.html)|**15**（~~24~~）|62|已售出
户外健将|[羽毛球拍（一对）](https://cakipaul.gitlab.io/2019selling/物品/羽毛球拍.html)+[握力器](https://cakipaul.gitlab.io/2019selling/物品/握力器.html)+[小哑铃](https://cakipaul.gitlab.io/2019selling/物品/小哑铃.html)+[头带](https://cakipaul.gitlab.io/2019selling/物品/头带.html)|**68**（~~87~~）|200|已售出
不可思议超值德智体美套餐|[吉他](https://cakipaul.gitlab.io/2019selling/物品/吉他.html)+[滑板](https://cakipaul.gitlab.io/2019selling/物品/滑板.html)+[篮球与排球](https://cakipaul.gitlab.io/2019selling/物品/球.html)|**600**（~~????~~）|????|已售出

>**可以加 Q 咨询议价~**

## 交易说明

- 交易范围：仅限山东大学青岛校区内部交易
- 付款方式：现金、支付宝、微信
- 提货地点：一多书院

## 联系方式

QQ：1836935533

QQ 群：615722490

Email：cakipaul@gmail.com

## 源代码

本站托管在 Gitlab 的 Pages 服务上：[Gitlab源代码](https://gitlab.com/cakipaul/2019selling)

大陆内 Coding 备份：[Coding源代码](https://dev.tencent.com/u/paulsun/p/2019selling/git)
